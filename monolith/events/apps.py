from django.apps import AppConfig


class EventsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "events"


# used line to check if the changes to the file are being saved and used by our Django development server.
# print("Did this restart?")
