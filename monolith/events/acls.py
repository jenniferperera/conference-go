from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

def get_photos(city, state):
    params = { "query": f"{city} {state}"}
    base_url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    response = requests.get(base_url, headers=headers, params=params)
    content = response.json()
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def weather_data(city, state):
# get lon and lat based on state, city.
    params = {"q": f"{city}, {state}, USA", "appid":OPEN_WEATHER_API_KEY}
    # wrote the try and except
    try:
        base_url = f"http://api.openweathermap.org/geo/1.0/direct"
        response = requests.get(base_url, params=params)
        content = response.json()
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]

    except (KeyError, IndexError):
        print("Key Error", KeyError)
        print("Index Error", IndexError)
        return None

    #get weather based on lon and lat.
    base_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": latitude, 
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
        }

    response = requests.get(base_url, params=params)
    content = response.json()
    description = content["weather"][0]["description"]
    temperature = content["main"]["temp"]
    return {"description": description, "temperature": temperature}
    
    
    # query = f"{city}, {state}"
    # base_url = pa
    #     "query": query
    # }
    # response = requests.get(base_url, headers=headers, context=context)
    # if response.status_code == 200:
    #     return response.json()["weather"]
    # else:
    #     return []
